package com.dstv.todo.observables;

import android.util.Log;

import com.dstv.todo.tasks.Task;

import java.util.List;
import java.util.Observable;

/**
 * Created by Mongez on 2017-04-13.
 */

public class ProgressObservable extends Observable {

    private static final String TAG = "ProgressObservable";

    private int mProgressStatus = 0;

    public void updateProgressToDoProgress(final List<Task> vTaskList){
        int vNumTasks = vTaskList.size();
        int vNumCompleteTasks = 0;
        mProgressStatus = 0;

        Log.d(TAG, "updateProgressToDoProgress: tasks - " + vNumTasks);

        if (vNumTasks > 0) {
            for (Task task : vTaskList) {
                if (task.isComplete()) {
                    vNumCompleteTasks += 1;
                } //End if: Task Complete
            } //End for: Task List

            Log.d(TAG, "updateProgressToDoProgress: completed tasks - " + vNumCompleteTasks);
            Log.d(TAG, "updateProgressToDoProgress: division - " + (vNumCompleteTasks / (float)vNumTasks));

            mProgressStatus = Math.round((vNumCompleteTasks / (float)vNumTasks) * 100); //TODO: Precision
        }

        Log.d(TAG, "updateProgressToDoProgress: status - " + mProgressStatus);

        setChanged();
        notifyObservers(mProgressStatus);
    }
}
