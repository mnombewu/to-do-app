package com.dstv.todo.tasks;

import java.io.Serializable;

/**
 * Represents Task entities
 *
 * Created by Mongez on 2017-04-12.
 */

public class Task implements Serializable {

    private int id;
    private String name;
    private String description;
    private boolean isComplete = false;

    public Task() {}

    public Task(String pName, String pDescription, boolean pStatus) {
        name = pName;
        description = pDescription;
        isComplete = pStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setIsComplete(boolean status) {
        this.isComplete = status;
    }
}