package com.dstv.todo;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.dstv.todo.listners.OnTaskItemSelectedListener;
import com.dstv.todo.tasks.Task;

public class ToDoActivity extends AppCompatActivity implements OnTaskItemSelectedListener, TaskEditFragment.OnTaskEditFragmentInteractionListener {
    private static final String TAG = "ToDoActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            ToDoActivityFragment vFragment = ToDoActivityFragment.newInstance();
            FragmentTransaction vTransaction = getSupportFragmentManager().beginTransaction();
            vTransaction.add(R.id.task_fragment_container, vFragment);
            //vTransaction.addToBackStack(null);
            vTransaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_to_do, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_more) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskSelected(Task pTask, int pPosition) {
        Log.d(TAG, "onTaskSelected: Begin");

        TaskEditFragment vFragment = TaskEditFragment.newInstance(pTask, pPosition);
        FragmentTransaction vTransaction = getSupportFragmentManager().beginTransaction();
        vTransaction.replace(R.id.task_fragment_container, vFragment);
        vTransaction.addToBackStack(null);
        vTransaction.commit();
    }

    @Override
    public void onTaskEdit(Task pTask, int pPosition) {
        Log.d(TAG, "onTaskEdit: Begin");

        ToDoActivityFragment vFragment = ToDoActivityFragment.newInstance(pTask, pPosition);
        FragmentTransaction vTransaction = getSupportFragmentManager().beginTransaction();
        vTransaction.replace(R.id.task_fragment_container, vFragment);
        vTransaction.addToBackStack(null);
        vTransaction.commit();
    }
}
