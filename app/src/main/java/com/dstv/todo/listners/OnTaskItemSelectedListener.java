package com.dstv.todo.listners;

import com.dstv.todo.tasks.Task;

/**
 * Created by Mongez on 2017-04-15.
 */

public interface OnTaskItemSelectedListener {

    void onTaskSelected(Task pTask, int pPosition);
}
