package com.dstv.todo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.dstv.todo.tasks.Task;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnTaskEditFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TaskEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TaskEditFragment extends Fragment implements View.OnClickListener{
    private static final String TAG = "TaskEditFragment";

    private static final String ARG_TASK = "task";
    private static final String ARG_TASK_POSITION = "position";

    private Task mTask;
    private int mPosition;
    private View mView;

    private OnTaskEditFragmentInteractionListener mListener;

    public TaskEditFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param pTask Parameter 1.
     * @param pPosition Parameter 2.
     * @return A new instance of fragment TaskEditFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TaskEditFragment newInstance(Task pTask, int pPosition) {
        Log.d(TAG, "newInstance: Begin");
        
        TaskEditFragment fragment = new TaskEditFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TASK, pTask);
        args.putInt(ARG_TASK_POSITION, pPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: Begin");
        
        if (getArguments() != null) {
            mTask = (Task) getArguments().getSerializable(ARG_TASK);
            mPosition = getArguments().getInt(ARG_TASK_POSITION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView: Begin");

        mView = inflater.inflate(R.layout.fragment_task_edit, container, false);
        Button btnTaskEdit = (Button) mView.findViewById(R.id.btn_task_edit);

        EditText vTxtName = (EditText) mView.findViewById(R.id.task_name);
        EditText vTxtDescription = (EditText) mView.findViewById(R.id.task_description);
        vTxtName.setText(mTask.getName());
        vTxtDescription.setText(mTask.getDescription());

        btnTaskEdit.setOnClickListener(this);

        // Inflate the layout for this fragment
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTaskEditFragmentInteractionListener) {
            mListener = (OnTaskEditFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTaskEditFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View pView) {
        switch (pView.getId()) {
            case R.id.btn_task_edit:
                updateTaskClick(pView);
                break;
        }
    }

    public void updateTaskClick(View pView) {
        Log.d(TAG, "updateTaskClick: Updating Task");

        //If no listener set then don't do anything
        if (mListener == null) {
            return;
        }

        View vParentView = (View) pView.getParent();
        EditText vTxtName = (EditText) vParentView.findViewById(R.id.task_name);
        EditText vTxtDescription = (EditText) vParentView.findViewById(R.id.task_description);

        String vName = vTxtName.getText().toString();
        String vDescription = vTxtDescription.getText().toString();

        if (vName.isEmpty()) {
            //No string entered
            return;
        }

        mTask.setName(vName);
        mTask.setDescription(vDescription);

        mListener.onTaskEdit(mTask, mPosition);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnTaskEditFragmentInteractionListener {
        void onTaskEdit(Task pTask, int pPosition);
    }
}
