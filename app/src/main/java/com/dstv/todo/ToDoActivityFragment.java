package com.dstv.todo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dstv.todo.adapters.TaskAdapter;
import com.dstv.todo.database.TaskDBContract;
import com.dstv.todo.database.TaskDBHelper;
import com.dstv.todo.listners.OnTaskItemSelectedListener;
import com.dstv.todo.observables.ProgressObservable;
import com.dstv.todo.tasks.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * A placeholder fragment containing a simple view.
 */
public class ToDoActivityFragment extends Fragment implements View.OnClickListener, Observer, OnTaskItemSelectedListener {
    private static final String TAG = "ToDoActivityFragment";

    private RecyclerView mTaskListView;
    private RecyclerView.Adapter mTaskAdapter;
    private RecyclerView.LayoutManager mTaskListLayoutManager;
    private View mView;
    ProgressObservable mProgressObservable;
    private ProgressBar mProgressBar;
    private TextView mProgressText;
    private static final String ARG_TASK = "task";
    private static final String ARG_TASK_POSITION = "position";
    private int mProgressStatus = 0;
    TaskDBHelper mDBHelper;
    private List<Task> mTasks = new ArrayList<>();
    DividerItemDecoration mDividerItemDecoration;
    OnTaskItemSelectedListener mTaskSelectedCallback;

    public static ToDoActivityFragment newInstance(Task pTask, int pPosition) {
        Log.d(TAG, "newInstance: Begin");

        ToDoActivityFragment vFragment = new ToDoActivityFragment();
        Bundle vArgs = new Bundle();
        vArgs.putSerializable(ARG_TASK, pTask);
        vArgs.putInt(ARG_TASK_POSITION, pPosition);
        vFragment.setArguments(vArgs);
        return vFragment;
    }

    public static ToDoActivityFragment newInstance() {
        Log.d(TAG, "newInstance: Begin");

        ToDoActivityFragment vFragment = new ToDoActivityFragment();
        return vFragment;
    }

    public ToDoActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTaskAdapter = new TaskAdapter(mTasks);
        mDBHelper  = new TaskDBHelper(getContext());
        //TODO: Move to better location, activity
        mProgressObservable = new ProgressObservable();

        //new ReadTasksFromDBAsync().execute();
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d(TAG, "onPause: Update Info");

        /* TODO: find better way of doing this if below is needed, database error on rotate screen
        TaskAdapter vTaskAdapter = (TaskAdapter)mTaskListView.getAdapter();
        mTasks = vTaskAdapter.getTaskList();
        Task[] vArray = mTasks.toArray(new Task[mTasks.size()]);
        new UpdateTasksToDB().execute(vArray);
        */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");

        mView = inflater.inflate(R.layout.fragment_to_do, container, false);
        Button btnToDo = (Button) mView.findViewById(R.id.btn_todo_add_new);
        ToDoActivity vToDoActivity = (ToDoActivity) getActivity();

        mTaskListView = (RecyclerView) mView.findViewById(R.id.task_list_view);
        mTaskListLayoutManager = new LinearLayoutManager(vToDoActivity);

        mTaskListView.setHasFixedSize(true);
        mTaskListView.setLayoutManager(mTaskListLayoutManager);
        mDividerItemDecoration = new DividerItemDecoration(mTaskListView.getContext(),
                LinearLayoutManager.VERTICAL);
        mTaskListView.addItemDecoration(mDividerItemDecoration);
        mTaskListView.setItemAnimator(new DefaultItemAnimator());
        mTaskListView.setAdapter(mTaskAdapter);

        //test data
        ((TaskAdapter) mTaskAdapter).clearAll();
        new ReadTasksFromDBAsync().execute();

        //listeners
        btnToDo.setOnClickListener(this);
        ((TaskAdapter) mTaskAdapter).setOnTaskItemSelectedListener(vToDoActivity);

        mProgressObservable.addObserver(this);
        mProgressObservable.addObserver((TaskAdapter) mTaskAdapter);
        ((TaskAdapter) mTaskAdapter).setProgressObservable(mProgressObservable);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
            new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                    Log.d(TAG, "onSwiped: Removing Task - " + viewHolder.getAdapterPosition());
                    TaskAdapter vTaskAdapter = (TaskAdapter)mTaskListView.getAdapter();
                    Task vTask = vTaskAdapter.remove(viewHolder.getAdapterPosition());

                    mProgressObservable.updateProgressToDoProgress(vTaskAdapter.getTaskList());
                    new DeleteTasksFromDB().execute(vTask);
                    //TODO: Add Undo functionality
                }
            };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mTaskListView);

        return mView;
    }

    public void btnAddTaskClick(View pView) {
        Log.d(TAG, "btnAddTaskClick: Adding Task");

        View vParentView = (View) pView.getParent();
        EditText vTxtName = (EditText) vParentView.findViewById(R.id.todo_add_new);
        Log.d(TAG, "btnAddTaskClick: Adding Task");
        String vName = vTxtName.getText().toString(); //TODO: null check

        if (vName.isEmpty()) {
            //No string entered
            return;
        }

        //clears user input
        vTxtName.getText().clear();

        TaskAdapter vTaskAdapter = (TaskAdapter)mTaskListView.getAdapter();

        Task vTask = new Task();
        vTask.setName(vName.trim());

        vTaskAdapter.add(vTask);
        new InsertTasksToDB().execute(vTask);

        mProgressObservable.updateProgressToDoProgress(vTaskAdapter.getTaskList());
    }

    @Override
    public void onClick(View pView) {
        switch (pView.getId()) {
            case R.id.btn_todo_add_new:
                btnAddTaskClick(pView);
                break;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Log.d(TAG, "update: " + arg);

        mProgressText = (TextView) mView.findViewById(R.id.progress_description);
        mProgressBar = (ProgressBar) mView.findViewById(R.id.task_list_progress);

        mProgressStatus = (int) arg;
        mProgressBar.setProgress(mProgressStatus);
        mProgressText.setText(mProgressStatus + "% Done"); //TODO: string constant interface/class

        TaskAdapter vTaskAdapter = (TaskAdapter)mTaskListView.getAdapter();
        mTasks = vTaskAdapter.getTaskList();
        Task[] vArray = mTasks.toArray(new Task[mTasks.size()]);
        new UpdateTasksToDB().execute(vArray);
    }

    @Override
    public void onDestroy() {
        mDBHelper.close();

        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mTaskSelectedCallback = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mTaskSelectedCallback = (OnTaskItemSelectedListener) context;
        } catch (ClassCastException ex) {
            throw new ClassCastException(context.toString()
                    + " must implement OnTaskItemSelectedListener");
        }
    }

    @Override
    public void onTaskSelected(Task pTask, int pPosition) {
        Log.d(TAG, "onTaskSelected: Selected dude");
        //TODO: Do something here?
    }

    private class  ReadTasksFromDBAsync extends AsyncTask<Void, Void, Void> {

        /**
         * Fetches list of tasks
         *
         * @ToDO Add exception handling
         * @param params
         * @return
         */
        @Override
        protected Void doInBackground(Void... params) {
            SQLiteDatabase vDB = mDBHelper.getReadableDatabase();

            String[] vColumns = {
                TaskDBContract._ID,
                TaskDBContract.COLUMN_NAME_TITLE,
                TaskDBContract.COLUMN_NAME_STATUS,
                TaskDBContract.COLUMN_NAME_DESCRIPTION
            };

            Cursor vCursor = vDB.query(
                TaskDBContract.TABLE_NAME,
                vColumns,
                null,
                null,
                null,
                null,
                null
            );

            while (vCursor.moveToNext()) {
                Task vTask = new Task();
                String vName = vCursor.getString(vCursor.getColumnIndex(TaskDBContract.COLUMN_NAME_TITLE));
                int vTStatus = vCursor.getInt(vCursor.getColumnIndex(TaskDBContract.COLUMN_NAME_STATUS));
                String vDescr = vCursor.getString(vCursor.getColumnIndex(TaskDBContract.COLUMN_NAME_DESCRIPTION));
                boolean vStatus = (vTStatus == 1) ? true : false;

                vTask.setName(vName);
                vTask.setIsComplete(vStatus);
                vTask.setDescription(vDescr);

                mTasks.add(vTask);
            }


            vCursor.close();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //If there are arguments provided to the fragment then update the Task list
            if (getArguments() != null && getArguments().getSerializable(ARG_TASK) != null) {
                Task vTask = (Task) getArguments().getSerializable(ARG_TASK);
                int vPosition = getArguments().getInt(ARG_TASK_POSITION);

                TaskAdapter vTaskAdapter = (TaskAdapter)mTaskListView.getAdapter();
                vTaskAdapter.set(vPosition, vTask);
                mTasks = vTaskAdapter.getTaskList();
                Task[] vArray = mTasks.toArray(new Task[mTasks.size()]);
                new UpdateTasksToDB().execute(vArray);
            }
        }
    }

    /**
     * Inserts a Task in to the DB
     */
    private class InsertTasksToDB extends AsyncTask<Task, Void, Void> {

        @Override
        protected Void doInBackground(Task... params) {
            SQLiteDatabase vDB = mDBHelper.getWritableDatabase();

            for (Task vTask: params) {
                if (vTask != null) {
                    ContentValues vValues = new ContentValues();
                    vValues.put(TaskDBContract.COLUMN_NAME_TITLE, vTask.getName());
                    vValues.put(TaskDBContract.COLUMN_NAME_STATUS, vTask.isComplete());

                    vDB.insert(
                        TaskDBContract.TABLE_NAME,
                        null,
                        vValues
                    );
                } //End if: Task null
            } //End for: tasks
            return null;
        }
    }

    /**
     * Deletes a Task from the DB
     */
    private class DeleteTasksFromDB extends AsyncTask<Task, Void, Void> {

        @Override
        protected Void doInBackground(Task... params) {
            SQLiteDatabase vDB = mDBHelper.getWritableDatabase();

            String vClause = TaskDBContract.COLUMN_NAME_TITLE + " LIKE ?";

            for (Task vTask: params) {
                if (vTask != null) {
                    String[] vArgs = {vTask.getName()};

                    vDB.delete(
                        TaskDBContract.TABLE_NAME,
                        vClause,
                        vArgs
                    );
                } //End if: Task null
            } //End for: tasks

            return null;
        }
    }

    /**
     * Updates the DB with the current Task list
     */
    private class UpdateTasksToDB extends AsyncTask<Task, Void, Void> {

        @Override
        protected Void doInBackground(Task... params) {
            SQLiteDatabase vDB = mDBHelper.getWritableDatabase();

            for (Task vTask: params) {
                if (vTask != null) {
                    ContentValues vValues = new ContentValues();
                    vValues.put(TaskDBContract.COLUMN_NAME_TITLE, vTask.getName());
                    vValues.put(TaskDBContract.COLUMN_NAME_STATUS, vTask.isComplete());
                    vValues.put(TaskDBContract.COLUMN_NAME_DESCRIPTION, vTask.getDescription());

                    String vClause = TaskDBContract.COLUMN_NAME_TITLE + " LIKE ?";
                    String[] vArgs = { vTask.getName() };

                    vDB.update(
                        TaskDBContract.TABLE_NAME,
                        vValues,
                        vClause,
                        vArgs
                    );
                } //End if: Task null
            } //End for: tasks

            return null;
        }
    }
}
