package com.dstv.todo.database;

import android.provider.BaseColumns;

/**
 * Created by Mongez on 2017-04-13.
 */

public final class TaskDBContract implements BaseColumns  {

    public static final String TABLE_NAME = "task";
    public static final String COLUMN_NAME_TITLE = "title";
    public static final String COLUMN_NAME_DESCRIPTION = "description";
    public static final String COLUMN_NAME_STATUS = "status";

    public static final String SQL_CREATE_TASK_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_NAME_TITLE + " TEXT NOT NULL," +
                    COLUMN_NAME_DESCRIPTION + " TEXT," +
                    COLUMN_NAME_STATUS + " INTEGER DEFAULT 0)";

    public static final String SQL_DELETE_TASK_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    private TaskDBContract()  {}

}
