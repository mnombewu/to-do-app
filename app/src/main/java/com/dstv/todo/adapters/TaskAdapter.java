package com.dstv.todo.adapters;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dstv.todo.R;
import com.dstv.todo.listners.OnTaskItemSelectedListener;
import com.dstv.todo.observables.ProgressObservable;
import com.dstv.todo.tasks.Task;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Mongez on 2017-04-12.
 */

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> implements Observer {
    private static final String TAG = "TaskAdapter Ninjah logs";

    private List<Task> mTaskList;
    ProgressObservable mProgressObservable;
    private OnTaskItemSelectedListener mOnTaskItemSelectedListener;

    public TaskAdapter(List<Task> pTasks) {
        mTaskList = pTasks;
    }

    public List<Task> getTaskList() {
        return mTaskList;
    }

    /**
     * Add an item to the list at a specific position
     *
     * @param pPosition Position of the item in the list
     * @param pItem Task to add
     */
    public void add(int pPosition, Task pItem) {
        mTaskList.add(pPosition, pItem);
        notifyItemInserted(pPosition);
    }

    /**
     * Set an item to the list at a specific position
     *
     * @param pPosition Position of the item in the list
     * @param pItem Task to add
     */
    public void set(int pPosition, Task pItem) {
        mTaskList.set(pPosition, pItem);
        notifyDataSetChanged();
    }

    /**
     * Add an item to the list
     *
     * @param pItem Task to add
     */
    public void add(Task pItem) {
        mTaskList.add(pItem);
        notifyDataSetChanged();
    }

    /**
     * Remove an item from the list
     *
     * @param pPosition Position of the item in the list
     */
    public Task remove(int pPosition) {
        Task vRemovedItem = mTaskList.remove(pPosition);
        notifyItemRemoved(pPosition);

        return vRemovedItem;
    }

    /**
     * Clears the list
     */
    public void clearAll() {
        mTaskList.clear();
        notifyDataSetChanged();
    }

    @Override
    public TaskAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: inflating");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_to_do, parent, false);
        ViewHolder vh = new ViewHolder(v);

        //CheckBox chkTaskStatus = (CheckBox) v.findViewById(R.id.todo_status);
        //chkTaskStatus.setOnClickListener(this);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: ");
        Task vTask = mTaskList.get(position);

        holder.mTxtName.setText(vTask.getName());
        holder.mTxtDescription.setText(vTask.getDescription());
        holder.mChkStatus.setChecked(vTask.isComplete());
    }

    @Override
    public int getItemCount() {
        return mTaskList.size();
    }

    @Override
    public void update(Observable o, Object arg) {
        //TODO
    }

    public void setProgressObservable(ProgressObservable mProgressObservable) {
        this.mProgressObservable = mProgressObservable;
    }

    public void setOnTaskItemSelectedListener(OnTaskItemSelectedListener mOnClickListener) {
        this.mOnTaskItemSelectedListener = mOnClickListener;
    }

    /**
     * View holder to get info from layout view
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mTxtName;
        public TextView mTxtDescription;
        public CheckBox mChkStatus;

        public ViewHolder(View pView) {
            super(pView);
            mTxtName = (TextView) pView.findViewById(R.id.todo_name);
            mTxtDescription = (TextView) pView.findViewById(R.id.todo_description);
            mChkStatus = (CheckBox) pView.findViewById(R.id.todo_status);

            pView.setOnClickListener(this);

            mChkStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d(TAG, "ViewHolder: Checked/Unchecked " + buttonView);

                    int vPosition = getAdapterPosition();
                    Task vSelectedTask = mTaskList.get(vPosition);
                    vSelectedTask.setIsComplete(isChecked);
                    mTaskList.set(vPosition, vSelectedTask);

                    mProgressObservable.updateProgressToDoProgress(mTaskList);

                    Log.d(TAG, "ViewHolder: List " + mTaskList.toString());
                }
            });

            Log.d(TAG, "ViewHolder: ");
        }

        @Override
        public void onClick(View v) {
            int vPosition = getAdapterPosition();
            Task vSelectedTask = mTaskList.get(vPosition);
            mOnTaskItemSelectedListener.onTaskSelected(vSelectedTask, vPosition);
        }
    }
}
